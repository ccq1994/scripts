# scripts

Useful scripts I reuse.

## gitlab-runner

These are scripts I've created over time as I work on a hands-off [GitLab runner](/gitlab-runner) in my home lab.

## `update`

A simple command I've added to be able to update (and clean updates on) my system easily.  It performs these steps:

- Update `apt` cache and list available updates
- Upgrade all `apt` packages
- Update all `flatpak` packages and clean up all unused packages
- Clean the `apt` cache
- Remove all unused packages from legacy dependencies

I've placed this at `~/.local/bin/update`.
You may need to modify your `~/.profile` to include the directory in your `$PATH`:

```bash
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
```