#!/bin/bash -x
if [[ -f /var/run/reboot-required ]]; then
	packages=$(cat /var/run/reboot-required.pkgs)
	logger -t "reboot-if-needed.sh" "Packages requiring reboot:"
	for package in $packages
	do
		logger -t "reboot-if-needed.sh" "    $package"
	done
	logger -t "reboot-if-needed.sh" "Initiating reboot."
	gitlab-runner stop
	reboot --force
fi