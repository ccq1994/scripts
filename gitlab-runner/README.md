# gitlab-runner Scripts

These scripts are useful for automatic maintenance on a GitLab runner.

## `reboot-if-needed.sh`

This script performs the following steps:

- Check if an Ubuntu-based system needs rebooted
- Log which packages are prompting the reboot
- Stop the `gitlab-runner` service
- Reboot the system

While my normal update `cron` job does perform a reboot upon its weekly run,
`unattended-upgrades` may install security packages which prompt for a reboot.
This script will reboot the system if such upgrades occur.

> Note: While [unattended-upgrades](https://help.ubuntu.com/community/AutomaticSecurityUpdates) can be configured to reboot automatically, I want to be extra careful with the GitLab runner service as to not interrupt any development.

To add this to the root user's crontab:

```bash
sudo chmod 754 reboot-if-needed.sh
sudo chown root:root reboot-if-needed.sh
sudo mv reboot-if-needed.sh /usr/local/sbin/
sudo crontab -e
```

Add the following line to the bottom of the page:

```cron
@hourly /usr/local/sbin/reboot-if-needed.sh
```

## Weekly update

This is just a `cron` entry to keep the system up-to-date.
Place it in the root user's crontab.

```cron
@weekly apt update && apt upgrade -y && gitlab-runner stop && reboot
```

## `daily-docker-cleanup.sh`

This script does a daily cleanup of all Docker volumes and images.
By its nature, GitLab runners use a lot of space for their runs.
To minimize the storage requirements and manual maintenance, this script does a daily cleanup.
On the first run of the day for each container image, it will take a few moments to download, but it will keep the disk from filling up after only a day or two.

This script takes a different approach to logging than `reboot-if-needed.sh`.
Rather than log to syslog, I want it to log to its own file.

To add this to the root user's crontab:

```bash
sudo chmod 754 daily-docker-cleanup.sh
sudo chown root:root daily-docker-cleanup.sh
sudo mv daily-docker-cleanup.sh /usr/local/sbin/
sudo crontab -e
```

Add the following line to the bottom of the page:

```cron
@daily /usr/local/sbin/daily-docker-cleanup.sh >> /var/log/docker_cleanup.txt 2>&1
```

## `config.toml`

While this isn't necessarily a script, it is a useful reference for configuring a runner with Docker executor.
After running the runner setup commands, there are several modifications I made:

- Allow concurrent runs (up to 2, the number of executors I have configured)
- Allow the Docker executors to access the Docker storage sockets
  - This is required for the Docker executor to actually function
- Set the pull policy to only download the container image if it's not already present on the system.
  - I don't update my container images often; the daily cleanup script makes sure my images are fresh enough for my purposes