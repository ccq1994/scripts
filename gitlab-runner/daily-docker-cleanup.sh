#!/bin/bash -x
date
gitlab-runner stop
/usr/local/sbin/clear-docker-cache
docker rmi -f $(docker images -a -q)
gitlab-runner start