# Gets a list of all logs, and gets all events in the past 30 minutes, then exports to a CSV
# Can change from AddMinutes to AddHours or AddDays if desired

Get-EventLog -List | ForEach-Object {
    Get-EventLog $_.Log -After (Get-Date).AddMinutes(-30) -ErrorAction SilentlyContinue
} | Export-Csv .\Documents\recent_logs.csv -NoTypeInformation
