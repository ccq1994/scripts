# PowerShell

## `Get-MsiProductIds.ps1`

This script is useful for finding the product ID codes of MSI packages.
It mounts a specified network share (can also be local),
reads all MSIs,
and writes out their name & product code.
It finishes by removing the drive it mounted.