if (-not(Test-Path Installers:)) {
    New-PSDrive -name Installers -PSProvider FileSystem -Root \\share\folder
}

Get-ChildItem Installers: | Where-Object Extension -EQ .msi |
ForEach-Object {
    $name = Write-Output $_.Name
    $product_code = (Get-AppLockerFileInformation $_.FullName | Select-Object -ExpandProperty Publisher).BinaryName
    Write-Output "$name`t$product_code"
}

Remove-PSDrive Installers