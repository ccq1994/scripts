# How to Output a Map with Sensitive Components

I'm writing this one for posterity,
as it took me quite a while to figure out.

## The Problem

Terraform has some challenges with `sensitive` values -
sometimes data will be treated as sensitive when you don't intend it to be,
and it can be hard to get the sensitive data to write out in clear text.
Ned Bellavance's [blog post](https://nedinthecloud.com/2022/06/29/nonsensitive-function-fails-in-terraform/)
helped me figure this out, but it only covered lists.
When I had a map that had a sensitive component, I had to poke at it a bit more.

In my case, I had AWS Parameter Store parameters that were not sensitive,
which I wanted to output.
The `value` attribute is [treated as sensitive](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter#value).
Combinations of marking the output as `sensitive = true` and using the `nonsensitive()` function
were not working on the map as a whole, but Ned's post helped me figure out what I needed to make nonsensitive.

Sometimes the error reported that I was redundant, sometimes it reported that it was sensitive.
```text
│ Error: Invalid function argument
│ 
│   on ../../../../terraform-modules/ssm-parameter-store/ssm-outputs.tf line 2, in locals:
│    2:   names  = nonsensitive(aws_ssm_parameter.default[*].name)
│     ├────────────────
│     │ while calling nonsensitive(value)
│     │ aws_ssm_parameter.default is tuple with 3 elements
│ 
│ Invalid value for "value" parameter: the given value is not sensitive, so this call is redundant.

│ Error: Output refers to sensitive values
│ 
│   on aggregator-main.tf line 49:
│   49: output "parameters" {
│ 
│ To reduce the risk of accidentally exporting sensitive data that was intended to be only internal, Terraform requires that any root module output containing sensitive data
│ be explicitly marked as sensitive, to confirm your intent.
│ 
│ If you do intend to export this data, annotate the output value as sensitive by adding the following argument:
│     sensitive = true
```

## The Fix

To fix this in my parameter module, I outputted the key:value map as follows:

```ruby
output "map" {
  value       = tomap({
    for p in aws_ssm_parameter.default : p.name => p.value
  })
  description = "A map of all the parameter names and values"
}
```

Then in the root module, I used `nonsensitive()` on only the value in a `for` statement.

```ruby
output "parameters" {
  description = "Map of parameters"
  value       = {for k, v in module.my_module.map : k => nonsensitive(v)}
}
```

And there we go!